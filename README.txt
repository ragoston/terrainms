For parallel programming class we were expected to come up with a problem that requires
a high computation time and implement a parallel solution later on in order to
ease up on execution time. My project is to generate/sculpt terrain in a 3D app, then import
it to WPF and draw its terrain in a 2D fashion, namely to show the height information with 2D lines.
I implemented the basic marching squares algo and then made it parallel using the master-worker pattern.

To test it out, pull either parallel or serial branch, start the program, hit enter, browse an obj (one is provided
with the name medTerrain.obj in the Algo project), and watch the magic happen. You can check the obj's look in any primary 3D app, visual studio or paint3D. 


